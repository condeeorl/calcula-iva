def precioProducto():
    print("¿Sobre que valor quieres calcular el precio con IVA?")
    try: 
        ProductoPrecio = float(input())
    except ValueError:
        print("Solo se admiten cantidades")
        return precioProducto()

    if isinstance(ProductoPrecio,int):
        Producto = int(ProductoPrecio)
        return Producto
    elif isinstance(ProductoPrecio,float):
        Producto = float(ProductoPrecio)
        return Producto
    else:   
        print("Solo se admiten cantidades")
        return precioProducto()
def TypeVat():
    print("¿ 1- Peninsula  2- Canarias?")
    typevat = input()
    if typevat.isdigit()== True:
        typeVat = int(typevat)
        if typeVat==1:
            return int(21)
        elif typeVat==2:
            return  float(6.5)
        else:
            print ("Por favor, elija 1 para Peninsula o 2 para Canarias")
            return TypeVat()
    else:
        print ("Por favor, elija 1 para Peninsula o 2 para Canarias")
        return TypeVat()

def precioFinal(Producto,VAT):
    precioFinal = (Producto * VAT)/ 100 + Producto
    return precioFinal
precio = precioProducto() 
iva = TypeVat()
print("Su precio es: ", precioFinal(precio,iva))
